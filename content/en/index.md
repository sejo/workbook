# Introduction

This is a work in progress...

## (Suggested) Outline
* Random systems
  * Chance operations
  * Probabilistic systems 
    * Weights
    * Markov Chains
* "Rigid" systems 
  * Permutations and combinations
    * Factorial and lexicographical order
    * Serialism
  * "Non corresponding" frequencies
    * Less common multiple
    * Counting phase shifts
    * Beating frequencies
    * Super Extremely Low Frequencies
  * Multiplexing
  * Fractals and recursion
    * Micro macrocosmic structures
    * Spiral - infinite fall
* Systems of emergent complexity (?)
  * Cellular automata
  * Computer
