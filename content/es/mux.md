# Multiplexación (_Multiplexing_)

Este término lo tomaremos prestado de la rama de telecomunicaciones.
La multiplexación consiste en combinar múltiples señales dentro de una sola, con el fin de compartir un medio de transmisión.
Existen diferentes formas de lograrlo, pero la que me interesa compartir aquí tiene que ver con división de tiempo:
_Time-Division Multiplexing_ o TDM.

Esta técnica se utiliza por ejemplo en las redes telefónicas: la cantidad de usuarios es tal, que en la búsqueda de eficacia múltiples llamadas se hacen pasan por el mismo cable.
Sin TDM, cuando una llamada se estuviera transmitiendo en un cable entre un punto A y un punto B, ninguna otra llamada entre esos dos puntos podría pasar; se requeriría un cable alterno.
Con TDM, lo que se hace es asignar fracciones de tiempo a cada llamada, para que cada una de forma ordenada utilice el cable durante esos momentos.
Las fracciones son tan cortas y la frecuencia con la que se repite el proceso es tan rápida, que como usuarios no notamos que eso sucede. 

Todo esto implica dos operaciones: 1) cada llamada (específicamente, cada señal de audio) es partida en bloques de corta duración, 2) dichos bloques son intercalados o alternados, consolidando una secuencia compuesta de bloques correspondientes a todas las llamadas involucradas.

(?)

_Diagramas_

Coreográficamente, el proceso podría plantearse de la siguiente forma:

* Construye N secuencias de movimiento
* Parte cada una en bloques de corta duración. ¿Poses? ¿Tiempos musicales? ¿Tiempos convencionales?
* Utiliza una estrategia para intercalar los bloques. Por ejemplo, orden constante, orden aleatorio, orden combinatorio...
* Ejecuta la recién construida "secuencia maestra"

## Demultiplexación
El proceso de la multiplexación tiene sentido de forma práctica cuando al final del canal de comunicación se usa el proceso opuesto:
la demultiplexación.
El sistema que demultiplexa tiene que conocer las reglas usadas al multiplexar, con el fin de poder extraer y reensamblar las distintas señales.

Para nuestros fines coreográficos, veo distintas formas de abordar la demultiplexación (y la consiguiente decodificación)

* Se puede ignorar completamente, y solo presentar la "secuencia maestra" como sujeto que se sostiene por sí solo, comentando o no el proceso detrás.
* Se pueden dar pistas durante la ejecución de la secuencia para permitir (al menos teóricamente) la demultiplexación por parte de la audiencia. Enunciados también fraccionados, luces distintas, números indicando la secuencia original a la que corresponde el bloque, cuentas en distintos idiomas, etc.
* Se pueden usar herramientas tecnológicas para literalmente demultiplexar la secuencia. Por ejemplo dispositivos que capturen fragmentos de video en los momentos correspondientes a cada bloque y los re-ensamblen como nuevos canales de video.
