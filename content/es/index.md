# Introducción

Escribo este libro por un lado como manual, por otro lado como manifiesto.
Hemos encontrado mundos inimaginados al desarrollar y llevar a escena algunas estrategias de composición coreográfica que aquí planteo.
Ha resultado interesante plantear estructuras a partir de sistemas "no-orgánicos", computacionales y numéricos, y observar las implicaciones en intérpretes y audiencia al intentar (y lograr) seguirlas.
Me interesa formalizar (o generalizar) esas estrategias para compartirlas e invitar a más personas a explorarlas:
Estoy seguro de que muchas sorpresas y reflexiones más nos aguardan al investigar estos métodos de creación.

_continuará..._



## Work in progress
Este libro es un trabajo en desarrollo; lo comparto para abrir el proceso de su creación.

Planeo trabajar en él diariamente, ya sea con el esqueleto de los capítulos, su desarrollo, ilustraciones, ejemplos y herramientas interactivas para explorar más a fondo los conceptos.

A continuación presento el _outline_ aproximado que he tenido en mente desde hace tiempo. 
Algunos conceptos los he explorado en escena, otros se han quedado en planteamientos, y los restantes siguen en espera de ser llevados a la realidad.


## Outline propuesto
* Sistemas aleatorios
  * Operaciones azarosas
  * Sistemas probabilísticos
    * Pesos
    * Cadenas de Markov
* Sistemas "rígidos"
  * Permutaciones y combinaciones
    * Factorial y orden lexicográfico
    * Serialismo
  * Frecuencias "no correspondientes"
    * Mínimo común múltiplo
    * Desfases contados
    * Frecuencias de Batimientos (beating frequencies)
    * Super Extremely Low Frequencies
  * Multiplexación
  * Fractales y recursividad
    * Estructuras micro-macro cósmicas
    * Espiral - caída infinita
* Sistemas de complejidad emergente (?)
  * Autómatas celulares
  * "Computador"
