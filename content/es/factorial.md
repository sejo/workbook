# Combinaciones y permutaciones

Muchas veces me pregunto sobre lo arbitrario que puede resultar el ordenamiento de elementos en una coreografía.
Podemos pensar en estos elementos a nivel espacial, por ejemplo personas en una fila, o a nivel temporal, como movimientos en una secuencia o secciones de una escena.
Probablemente hay muchas razones detrás de la elección de cierto ordenamiento, pero siempre queda la duda de si ese era realmente el "mejor", o de si no habría otros igual de buenos.

Encontrar ordenamientos alternativos podría ser útil para complejizar estructuras - qué tal si esas secuencias se ejecutan al mismo tiempo para desarrollar un contrapunto, o qué tal si cada función presenta una secuencia distinta de escenas.
O en un caso más extremo, qué tal si todos los posibles ordenamientos se usan como único material coreográfico; 
a continuación veremos cómo un conjunto relativamente reducido de elementos nos puede llevar hacia el _infinito_.
(¿y qué no algo así es el sueño minimalista, de construir mucho con poco?)

## Factorial y permutaciones sin repetición

Dados N elementos, existen _N!_ (N factorial) formas de ordenarlos.
_N!_ es igual a la multiplicación consecutiva de los números naturales desde 1 hasta N (?).
Por ejemplo, 5! es igual a 1 por 2 por 3 por 4 por 5, que es 120.

En este caso, con "ordenarlos" me refiero al hecho de usar _todos_ los elementos en cada acomodo, y solo una vez por acomodo.
De manera más técnica, estamos hablando de _permutaciones sin repetición_.
Si pensamos como ejemplo en los posibles ordenamientos de un número de personas en una fila, cada persona solo puede usar un lugar en cada permutación dada.

### ¿Cómo se construye el factorial?

Es útil pensar a partir de los casos más simples. 
Empecemos con una fila de una persona: Ana. 
Esa fila solo tiene un (1) posible ordenamiento.

_Imagen: Ana_

Ahora pensemos en una fila que tiene a dos personas, Ana y Benito.
Una persona puede ocupar el primer lugar mientras la otra ocupa el segundo, o viceversa.
Esto nos da dos posibles ordenamientos.

Estudiemos el ejemplo a mayor detalle...
Visto de otra manera, para encontrar los posibles ordenamientos podemos decir: 
Si colocamos a Ana al inicio de la fila, ¿de cuántas maneras podemos ordenar a la fila restante?
La fila restante consiste solo en Benito, y ya sabemos que una fila de una persona solo tiene un posible ordenamiento.
Por lo tanto, con Ana al inicio, solo hay una posibilidad.
De forma similar, podemos decir:
Si colocamos a Benito al incio de la fila, ¿de cuántas maneras podemos ordenar a la fila restante?
En esta ocasión la fila restante consiste en Ana, y de nuevo esta fila solo tiene un posible ordenamiento.
Por lo tanto, con Benito al inicio, también solo hay una posibilidad.

Tenemos a dos (_2_) personas, y por cada una al inicio de la fila, hay un (_1!_) solo ordenamiento posible.
Esto implica que con dos personas en total tenemos _2 por 1_, o _2!_ ordenamientos posibles.

_Imagen:_

_Ana y Benito_

_Benito y Ana_

Esta forma de razonar, si bien pudo parecer obvia, nos será útil a continuación.
Pensemos en que ahora tenemos a tres personas, Ana, Benito y Cecilia.
¿Cuántos ordenamientos existen?

Si colocamos a Ana al inicio de la fila, ¿de cuántas maneras podemos ordenar a la fila restante?
La fila restante consiste en Benito y Cecilia. 
Ya sabemos que a ellos los podemos ordenar de dos maneras distintas, y aunque no lo supiéramos, podríamos repetir el proceso
(_Si colocamos a Benito al inicio de la fila restante, ¿de cuántas maneras podemos ordenar a la fila que ahora restaría?_).

Ya que sabemos el caso de Ana, podemos entonces construir o inferir lo siguiente:
Con Ana al inicio, hay dos ordenamientos posibles, con Benito al inicio, también hay dos ordenamientos posibles, y finalmente,
con Cecilia al inicio, también hay dos ordenamientos posibles.
Es decir, tenemos a tres (_3_) personas, y por cada una al inicio de la fila, hay dos (_2!_) ordenamientos posibles.
Esto implica que con una fila de tres personas, en total tenemos _3 por 2 por 1_, _3!_, o _6_ ordenamientos posibles.

_Imagen:_

_Ana, Benito y Cecilia_

_Ana, Cecilia y Benito_

_Benito, Ana y Cecilia_

_Benito, Cecilia y Ana_

_Cecilia, Ana y Benito_

_Cecilia, Benito y Ana_

A partir de esta base, es posible aumentar el número de personas en la fila.
Por ejemplo, si tenemos a cuatro (_4_) personas, sabemos que por cada una al inicio de la fila, hay seis (_3!_) ordenamientos posibles.
Esto significa que en total hay _4!_, _4 por 3!_ o _4 por 3 por 2 por 1_, _24_ ordenamientos posibles.
Y si tenemos a cinco (_5_) personas, el número de ordenamientos será _5!_ que es _120_.


### N! coreográfico 

A partir del ejemplo anterior de cinco personas con 120 maneras de acomodarlas en una fila, 
podemos involucrar el factor tiempo y preguntarnos cuánto tardaríamos en pasar por todas esas filas posibles.
Si, por decir algo, cada transición entre permutación tomara cinco segundos, entonces requeriríamos 600 segundos para realizar todas.
600 segundos son 10 minutos: ¿no suena atractiva una coreografía de corta duración que solo consista en 5 personas realizando todas las permutaciones de fila posibles?

¿Qué pasa si agregamos a una persona?
6! es 1 por 2 por 3 por 4 por 5 por 6, o bien 5! por 6. El resultado ahora es 720 permutaciones.
Si con 5 personas necesitábamos 10 minutos, ¿cuánto tiempo necesitamos ahora con 6?
720 por 5 segundos, o bien, 10 minutos (tiempo de 5!) por 6, resulta en 3600 segundos o 60 minutos: ¡1 hora!
¡Solo agregando una persona más, ya tenemos una coreografía de largo formato!

Y si siguiéramos así, ¡para pasar por todas las permutaciones con siete personas necesitaríamos 7 horas, con ocho personas 56 horas, con nueve personas 21 días, con diez personas 210 días y con once personas más de 6 años!

## Orden lexicográfico

...
