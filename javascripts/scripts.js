window.onload = function(){
var tocToggle = document.getElementById('tocToggle');
var toc = document.getElementById('tocContainer');

if(tocToggle && toc) {

  // Toggle TOC on click
  tocToggle.addEventListener('click', function(e) {
    e.preventDefault();
    if(toc.style.display == 'block') {
      toc.style.display = 'none';
    }
    else {
      toc.style.display = 'block';
    }
  });
}

}

