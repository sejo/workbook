# Workbook of Computational and Numerical Choreography

[workbook.eccs.world](http://workbook.eccs.world)

Built with [Magic Book](https://github.com/magicbookproject/magicbook)

Template loosely based in the [Programming Design Systems book](https://github.com/runemadsen/programmingdesignsystems.com)
